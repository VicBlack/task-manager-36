package ru.t1.kupriyanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.command.AbstractCommand;
import ru.t1.kupriyanov.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public final class ApplicationArgumentListCommand extends AbstractSystemCommand {

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        if (commands == null) throw new CommandNotSupportedException();
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) continue;
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show argument list.";
    }

}
