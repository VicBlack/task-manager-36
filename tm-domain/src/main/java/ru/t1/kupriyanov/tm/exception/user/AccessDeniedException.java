package ru.t1.kupriyanov.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! Access is denied!");
    }

    public AccessDeniedException(int n) {
        super("Error! Access is denied! number: " + n);
    }

}
