package ru.t1.kupriyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskStartByIndexRequest extends AbstractIndexRequest {

    public TaskStartByIndexRequest(@Nullable String token, @Nullable Integer index) {
        super(token, index);
    }

}
